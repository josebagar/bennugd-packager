**Please note that this is very old code that serves no real purpose anymore.**

This project holds the source code for my BennuGD Packager. Right now it tries to get in charge of packaging your BennuGD game for Android.

The code is mostly written in Python and makes use of PyQt4 for the GUI.

Binaries for Windows are provided in the downloads section and Ubuntu users can usually grab the latest binaries in my PPA: https://launchpad.net/~josebagar/+archive/ppa

OSX is theoretically supported -and I do run the packager there from time to time- through Python.org binaries (not OSX-bundled ones) but users have not expressed much interest, so no binaries are provided.

The source code for this tool used to reside at its sister project page (https://code.google.com/p/bennugd-monolithic/) but has since moved here.